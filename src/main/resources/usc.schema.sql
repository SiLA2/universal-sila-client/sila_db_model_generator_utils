--------------------------------------------------------------------------------
--	              UNIVERSAL SILA CLIENT H2 DATABASE
--							H2 DATABASE
--						(16/5/2021 0:51:44)
--------------------------------------------------------------------------------
--	DATABASE NAME : USC_DATABASE
--	BASE LABEL : Universal SiLA Client Relational Database
--	PROJECT : universal-sila-client
--	AUTHOR : Jeremy Tran
--	REVIEWER : Timothy Diguiet
--	LAST MODIFICATION DATE : 16/5/2021 1:53:42
--------------------------------------------------------------------------------

-- DROP DATABASE USC_DATABASE;
--------------------------------------------------------------------------------
--		CREATION OF DATABASE
--------------------------------------------------------------------------------

-- CREATE DATABASE USC_DATABASE;

--------------------------------------------------------------------------------
--		TABLE : CLIENT
--------------------------------------------------------------------------------

CREATE TABLE CLIENT (
                        CLIENT_UUID UUID NOT NULL,
                        NAME VARCHAR(255) NOT NULL,
                        CONSTRAINT PK_CLIENT PRIMARY KEY (CLIENT_UUID)
);

--------------------------------------------------------------------------------
--		TABLE : SERVER
--------------------------------------------------------------------------------

CREATE TABLE SERVER (
                        SERVER_UUID UUID NOT NULL,
                        JOINED DATE NOT NULL,
                        STATUS CHAR(32) NOT NULL CHECK (STATUS IN ('OFFLINE', 'ONLINE')),
                        NEGOTIATION_TYPE CHAR(32) NOT NULL CHECK (NEGOTIATION_TYPE IN ('PLAIN_TEXT', 'TLS')),
                        SERVER_HOST VARCHAR(255) NOT NULL,
                        PORT INT4 NOT NULL,
                        NAME VARCHAR(255) NOT NULL,
                        SERVER_TYPE VARCHAR(255) NOT NULL,
                        DESCRIPTION VARCHAR NOT NULL,
                        VENDOR_URL VARCHAR(255) NOT NULL,
                        VERSION VARCHAR(255) NOT NULL,
                        CONSTRAINT PK_SERVER PRIMARY KEY (SERVER_UUID)
);

--------------------------------------------------------------------------------
--		TABLE : METADATA
--------------------------------------------------------------------------------

CREATE TABLE METADATA (
                          ID INT8 NOT NULL,
                          FEATURE_ID INT8 NOT NULL,
                          RELATIVE_IDENTIFIER VARCHAR(255) NOT NULL,
                          DISPLAY_NAME VARCHAR(255) NOT NULL,
                          DESCRIPTION VARCHAR NOT NULL,
                          DATA_TYPE JSON NOT NULL,
                          CONSTRAINT PK_METADATA PRIMARY KEY (ID)
);

--------------------------------------------------------------------------------
--		INDEX OF METADATA TABLE
--------------------------------------------------------------------------------

CREATE INDEX I_FK_METADATA_FEATURE
	ON METADATA (FEATURE_ID);

--------------------------------------------------------------------------------
--		TABLE : COMMAND
--------------------------------------------------------------------------------

CREATE TABLE COMMAND (
                         ID INT8 NOT NULL,
                         FEATURE_ID INT8 NOT NULL,
                         RELATIVE_IDENTIFIER VARCHAR(255) NOT NULL,
                         DISPLAY_NAME VARCHAR(255) NOT NULL,
                         DESCRIPTION VARCHAR NOT NULL,
                         OBSERVABLE BOOL NOT NULL,
                         EXECUTION_ERRORS JSON NOT NULL,
                         CONSTRAINT PK_COMMAND PRIMARY KEY (ID)
);

--------------------------------------------------------------------------------
--		INDEX OF COMMAND TABLE
--------------------------------------------------------------------------------

CREATE INDEX I_FK_COMMAND_FEATURE
	ON COMMAND (FEATURE_ID);

--------------------------------------------------------------------------------
--		TABLE : PROPERTY
--------------------------------------------------------------------------------

CREATE TABLE PROPERTY (
                          ID INT8 NOT NULL,
                          FEATURE_ID INT8 NOT NULL,
                          RELATIVE_IDENTIFIER VARCHAR(255) NOT NULL,
                          DISPLAY_NAME VARCHAR(255) NOT NULL,
                          DESCRIPTION VARCHAR NOT NULL,
                          OBSERVABLE BOOL NOT NULL,
                          DATA_TYPE JSON NOT NULL,
                          CONSTRAINT PK_PROPERTY PRIMARY KEY (ID)
);

--------------------------------------------------------------------------------
--		INDEX OF PROPERTY TABLE
--------------------------------------------------------------------------------

CREATE INDEX I_FK_PROPERTY_FEATURE
	 ON PROPERTY (FEATURE_ID);

--------------------------------------------------------------------------------
--		TABLE : CALL
--------------------------------------------------------------------------------

CREATE TABLE CALL (
                      ID INT8 NOT NULL,
                      COMMAND_ID INT8 NOT NULL,
                      CLIENT_UUID UUID NOT NULL,
                      RELATIVE_IDENTIFIER VARCHAR(255) NOT NULL,
                      PARAMETERS JSON NOT NULL,
                      START_DATE DATE NOT NULL,
                      END_DATE DATE NOT NULL,
                      RESULT VARCHAR NOT NULL,
                      TIMEOUT JSON NOT NULL,
                      TYPE CHAR(32) NOT NULL CHECK (TYPE IN ('UNOBSERVABLE_COMMAND', 'OBSERVABLE_COMMAND', 'UNOBSERVABLE_PROPERTY', 'OBSERVABLE_PROPERTY')),
                      CONSTRAINT PK_CALL PRIMARY KEY (ID)
);

--------------------------------------------------------------------------------
--		INDEX OF CALL TABLE
--------------------------------------------------------------------------------
CREATE INDEX I_FK_CALL_COMMAND
	ON CALL (COMMAND_ID);

CREATE INDEX I_FK_CALL_CLIENT
	ON CALL (CLIENT_UUID);

--------------------------------------------------------------------------------
--		TABLE : PARAMETER
--------------------------------------------------------------------------------

CREATE TABLE PARAMETER (
                           ID INT8 NOT NULL,
                           COMMAND_ID INT8 NOT NULL,
                           RELATIVE_IDENTIFIER VARCHAR(255) NOT NULL,
                           DISPLAY_NAME VARCHAR(255) NOT NULL,
                           DESCRIPTION VARCHAR NOT NULL,
                           DATA_TYPE JSON NOT NULL,
                           CONSTRAINT PK_PARAMETER PRIMARY KEY (ID)
);

--------------------------------------------------------------------------------
--		INDEX OF PARAMETER TABLE
--------------------------------------------------------------------------------

CREATE INDEX I_FK_PARAMETER_COMMAND
	ON PARAMETER (COMMAND_ID);

--------------------------------------------------------------------------------
--		TABLE : FEATURE
--------------------------------------------------------------------------------

CREATE TABLE FEATURE (
                         ID INT8 NOT NULL,
                         SERVER_UUID UUID NOT NULL,
                         RELATIVE_IDENTIFIER VARCHAR(255) NOT NULL,
                         DISPLAY_NAME VARCHAR(255) NOT NULL,
                         DESCRIPTION VARCHAR NOT NULL,
                         LOCALE VARCHAR(255) NOT NULL,
                         SILA2_VERSION VARCHAR(255) NOT NULL,
                         FEATURE_VERSION VARCHAR(255) NOT NULL,
                         MATURITY_LEVEL VARCHAR(255) NOT NULL,
                         ORIGINATOR VARCHAR(255) NOT NULL,
                         CATEGORY VARCHAR(255) NOT NULL,
                         CONSTRAINT PK_FEATURE PRIMARY KEY (ID)
);

--------------------------------------------------------------------------------
--		INDEX OF FEATURE TABLE
--------------------------------------------------------------------------------

CREATE INDEX I_FK_FEATURE_SERVER
	ON FEATURE (SERVER_UUID);

--------------------------------------------------------------------------------
--		TABLE : BOOKMARK
--------------------------------------------------------------------------------

CREATE TABLE BOOKMARK (
                          SERVER_UUID UUID NOT NULL,
                          CLIENT_UUID UUID NOT NULL,
                          CONSTRAINT PK_BOOKMARK PRIMARY KEY (SERVER_UUID, CLIENT_UUID)
);

--------------------------------------------------------------------------------
--		INDEX OF BOOKMARK TABLE
--------------------------------------------------------------------------------

CREATE INDEX I_FK_BOOKMARK_SERVER
	ON BOOKMARK (SERVER_UUID);

CREATE INDEX I_FK_BOOKMARK_CLIENT
	ON BOOKMARK (CLIENT_UUID);

--------------------------------------------------------------------------------
--		CREATION OF TABLE REFERENCES
--------------------------------------------------------------------------------

ALTER TABLE METADATA ADD
    CONSTRAINT FK_METADATA_FEATURE
        FOREIGN KEY (FEATURE_ID)
            REFERENCES FEATURE (ID);

ALTER TABLE COMMAND ADD
    CONSTRAINT FK_COMMAND_FEATURE
        FOREIGN KEY (FEATURE_ID)
            REFERENCES FEATURE (ID);

ALTER TABLE PROPERTY ADD
    CONSTRAINT FK_PROPERTY_FEATURE
        FOREIGN KEY (FEATURE_ID)
            REFERENCES FEATURE (ID);

ALTER TABLE CALL ADD
    CONSTRAINT FK_CALL_COMMAND
        FOREIGN KEY (COMMAND_ID)
            REFERENCES COMMAND (ID);

ALTER TABLE CALL ADD
    CONSTRAINT FK_CALL_CLIENT
        FOREIGN KEY (CLIENT_UUID)
            REFERENCES CLIENT (CLIENT_UUID);

ALTER TABLE PARAMETER ADD
    CONSTRAINT FK_PARAMETER_COMMAND
        FOREIGN KEY (COMMAND_ID)
            REFERENCES COMMAND (ID);

ALTER TABLE FEATURE ADD
    CONSTRAINT FK_FEATURE_SERVER
        FOREIGN KEY (SERVER_UUID)
            REFERENCES SERVER (SERVER_UUID);

ALTER TABLE BOOKMARK ADD
    CONSTRAINT FK_BOOKMARK_SERVER
        FOREIGN KEY (SERVER_UUID)
            REFERENCES SERVER (SERVER_UUID);

ALTER TABLE BOOKMARK ADD
    CONSTRAINT FK_BOOKMARK_CLIENT
        FOREIGN KEY (CLIENT_UUID)
            REFERENCES CLIENT (CLIENT_UUID);